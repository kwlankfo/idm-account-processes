Write-Host "Make sure you have activated your User Administrator role in Azure"
Write-Host "Once role is verified, press any key to continue..."
$x = $host.UI.RawUI.ReadKey("NoEcho,IncludeKeyDown")
Connect-msolservice
Write-Host "If new and old usernames end in @uncg.edu, do not include the @uncg.edu."
Write-Host "If new and old usernames end in something other than @uncg.edu, include the @ domain to the username."
$input1 = Read-Host -Prompt 'Input Old Username' 
$atpos = $input1.LastIndexOf("@")
if ($atpos -gt 0)
{
	$olduname = $input1
}
else
{
	$olduname = $input1 + "@uncg.edu"
}
$input2 = Read-Host -Prompt 'Input New Username'
$atpos = $input2.LastIndexOf("@")
if ($atpos -gt 0)
{
	$newuname = $input2
}
else
{
	$newuname = $input2 + "@uncg.edu"
}
Set-MsolUserPrincipalName -UserPrincipalName "$olduname" -NewUserPrincipalName "$newuname"
